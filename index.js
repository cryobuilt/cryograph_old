var DS = require('dslink');
var AWS = require('aws-sdk');


// Process the arguments and initializes the default nodes.
var link = new DS.LinkProvider(process.argv.slice(2), 'cryograph-', {
	commandLineOptions: {
		aws_access_key_id: '',
		aws_secret_access_key: ''
	},
	defaultLogLevel: 'debug',
	defaultNodes: {
		Store: {
			$type: 'string',
			'?value': '',
			$writable: "write",
		},
	}
});

var dynamodb;
link.init();
link.connect().then(function() {
	dynamodb = new AWS.DynamoDB.DocumentClient({
		region: 'us-east-1',
		accessKeyId: link.getCommandLineValue('aws_access_key_id'),
		secretAccessKey: link.getCommandLineValue('aws_secret_access_key')
	});
	link.getNode('/Store').subscribe(function(update) {
		subscribeUpdates(update, 'store');
		link.save();
	});
})
.catch(function(e) {
	console.log(e.stack);
});

function subscribeUpdates(update, name) {
	//console.log('Name: ' + name + ' ValueUpdate: ' + update.value + ' ' + update.timestamp + ' ' + update.ts);
	var d = new Date(update.ts);
	try {
		var time = Math.floor(d.getTime()/1000);
		var payload = JSON.parse(update.value);
		var thislocation = payload.Location;
		var params = {
			Item: {
				Time: time,
				Location: thislocation,
				payload: payload
			},
			TableName: 'Trend'
		};

		dynamodb.put(params, function (err, data) {
			if (err) {
				console.log(err, err.stack); // an error occurred
			} else  {
				//console.log(data);           // successful response
			}
		});
	} catch (e) {
		console.log(e);
	}
}



